
# BISM API Documentation

 
Instrumentation is implemented in a Transformer class. To implement a new Transformer, you need to create separately a new Java class that extends `inria.bism.transformers.Transformer` from the `bism.jar`.  Here is an example of a transformer class:

```java
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;

public class IteratorTransformer extends Transformer {
 @Override
 public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc){
    // Filter to only method of interest
    if (mc.methodName.equals("iterator") && mc.methodOwner.endsWith("List")) {
       
        // Access to dynamic data
        DynamicValue list = dc.getMethodTarget();
        DynamicValue iterator = dc.getMethodResult();   
       
        // Create an ArrayList in the target method
        LocalArray la = dc.createLocalArray(mc.ins.basicBlock.method, Object.class);
        dc.addToLocalArray(la,list);
        dc.addToLocalArray(la,iterator);

        // Invoke the monitor after passing arguments
        StaticInvocation sti = 
            new StaticInvocation("SafeListMonitor", "receive");
        sti.addParameter("create");          
        sti.addParameter(la);
        invoke(sti);
    }
 } 
}
```
The BISM transformer, written in Java, uses the selector `afterMethodCall` to capture the return of an  `Iterator` created from a  `List.iterator()`  method call.
It uses the dynamic context object provided to retrieve the associated objects with the event, and pushes them into a list.
Then, invokes a monitor passing the extracted information.




The sections below describe the Transformer API. How instrumentation can be achieved, what join points are available, and what static and dynamic context is available at each join point.


##  Advice Methods  

A user inserts advice into the base program at the captured joinpoints using the advice instrumentation methods. Advice methods allow the user to extract needed static and dynamic information from within joinpoints, also allowing arbitrary bytecode insertion. These methods are invoked within selectors.  

We list below all the transformer api methods:



###  Printing on Console

These methods are used to instrument print commands.

```void println(String message)```

Used to print a message on the console and a new line after

```void print(String message)```

Used to print a message on the console

```void print(DynamicValue dv)```

Used to print the toString() of a dynamic value (local variable) on the console.

```void printHash(DynamicValue dv)```

Used to print the System.identityHashCode() of a dynamic value (local variable).
This is useful for getting unique ids for objects.


```void print(String message, boolean err)```

Same as *print(String message)*  but if passed boolean true, the print stream will be ```err```.

```void print(DynamicValue dv, boolean err)```

Same as *print(DynamicValue dv)* but if passed boolean true, the print stream will be ```err```.

```void printHash(DynamicValue dv, boolean err)```

Same as *printHash(DynamicValue dv)* but if passed boolean true, the print stream will be ```err```.

Below is an example that prints the method name and result after a method call:
```java
@Override
public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

    DynamicValue result = dc.getMethodResult(methodCall);

    print(mc.methodName + " result:");  //print message
    print(result);  //print a dynamic value
}
```


### Method Invocation


This method is used to instrument method invocation.

```void invoke(StaticInvocation smi)```

Used to invoke a static method. Methods can be external, note that to run the instrumented code, the method owner class should be included in the classpath. ```StaticInvocation``` class should be passed to ```invoke```. The datatype is simple. The constructor takes in class name and method name. ```addParameter()``` is then called to add parameters to the onvocation.
  
  Below is an example of invoking a static method (representing a monitor) and passing some dynamic ans static context:

```java
@Override
public void beforeMethodCall(MethodCall methodCall, MethodCallDynamicContext dc) {

    if (!methodCall.methodName.equals("callMe"))  return;

    DynamicValue p1 = dc.getAllMethodArgs(methodCall); //get all method arguments
   
    // pass in full class name and method name
    StaticInvocation sti = new StaticInvocation("test/IteratorTest", "monitorCallMe");
    sti.addParameter(p1); // pass the dynamic value
    sti.addParameter(5);  //pass a primitve type

    invoke(sti);  //invoke
}
```

The ```addParameter``` only accepts either a ```DynamicValue``` or a primitive type. Any other type will be ignored.

### Bytecode Instructions Insertion


These methods are used to instrument bytecode instructions.

 ```insert(AbstractInsnNode ins)``` 

Used for manually inserting an ASM bytecode instructions.

```insert(List<AbstractInsnNode> ins)```

Used for manually inserting a list of ASM bytecode instructions.

Below is an example of inserting a DUP instruction in order to reevaluate
a conditional jump after the jump occurs:
```java
@Override
public void beforeInstruction(Instruction ins, InstructionDynamicContext dc) {
    if (ins.isConditionalJump) {
        if (ins.stackOperandsCountIfConditionalJump == 1)
            insert(new InsnNode(Opcodes.DUP)); //insert DUP
        else
            insert(new InsnNode(Opcodes.DUP2)); //insert DUP2
    }
}
  ```

  ----
  ## Selectors

Selectors provide a mechanism to select joinpoints and specify the advice. They are implementable methods where the user writes the instrumentation logic. BISM provides a fixed set of selectors classified into four categories: instruction, basic block, method, and meta selectors. We list below the set of available selectors and specify the execution they capture. A transformer can implement one or many selectors by overriding one of the below methods. Each overridden selector can invoke methods of the ```Advice Methods``` to append the source code . We list the available selectors and specify where the instrumented code will be executed:


```java
void beforeInstruction(Instruction ins, InstructionDynamicContext dc);
void afterInstruction(Instruction ins, InstructionDynamicContext dc);
void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc);
void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc);
void beforeSetField(FieldAccess fa, FieldSetDynamicContext dc);
void afterGetField(FieldAccess fa, FieldGetDynamicContext dc);
void onBasicBlockEnter(BasicBlock bb, InstructionDynamicContext dc);
void onBasicBlockExit(BasicBlock bb, InstructionDynamicContext dc);
void onTrueBranchEnter(BasicBlock bb, InstructionDynamicContext dc);
void onFalseBranchEnter(BasicBlock bb, InstructionDynamicContext dc);
void onMethodEnter(Method m, MethodDynamicContext dc);
void onMethodExit(Method m, MethodDynamicContext dc);
void onClassEnter(ClassContext c);

```

* ```Before Instruction```
executes before a bytecode instruction. If the instruction is the entry point of a basic block, the code executes after the instruction.

* ```After Instruction```
executes after a bytecode instruction. If the instruction is the exit point of a basic block, the code executes before the instruction.

* ```Before Method Call```
executes before a method call, after loading any needed values on the stack.

* ```After Method Call```
executes immediately after a method call, before storing or popping the return value from the stack if any.
* ```Before SetField``` 
executes when a value is assigned to a field.
* ```After GetField``` 
executes when a field's value is accessed or retrieved.

* ```On Basic Block Enter```
executes after the entry Label of the basic block.
* ```On Basic Block Exit```
executes after the last instruction of a basic block; except when last instruction is a JUMP/RETURN/THROW instruction, then it executes before the last instruction.
* ```On True Branch Enter```
executes on the entry of the True successor block (applies only to conditional jumps).
* ```On False Branch Enter```
executes on the entry of the False successor block (applies only to conditional jumps).
* ```On Method Enter```
executes on method entry block, rules of Before Basic Block apply here.
* ```On Method Exit```
executes on all exit blocks before the return or throw instruction.
* ```On Class Enter```
allows a developer to insert class fields

----
## Static Context

Static-context objects provide access to relevant static in- formation for captured joinpoints in selectors. Each selector has a specific static context object based on its category. These objects can be used to retrieve information about byte-code instructions, method calls, basic blocks, methods, and classes.

**```Instruction```** provides all relevant information about one instruction, and it contains the following fields:
* **index**:  a unique instruction index
* **node**:  the ASM ```org.objectweb.asm.tree.AbstractInsNode``` that can be casted into a more specific ASM AbstractInsNode sub type
* **opcode**: the bytecode instruction opcode
* **next**: the next instruction in the basic block, null if at the end of a block
* **previous**: the previous instruction in the basic block, null if at the beginning of a basic block
* **isConditionalJump()**: true if instruction is a conditional jump instruction
* **isBranchingInstruction()**: true if instruction is instanceof JumpInsnNode, LookupSwitchInsnNode, TableSwitchInsnNode; or opcode is ATHROW, RET, IRETURN, RETURN)
* **stackOperandsCountIfConditionalJump()**: the number of stack operands a conditional jump requires, else -1 for non conditional jumps
* **getBasicValueFrame()**: contains a list of all local variables and stack items and their types
* **getSourceValueFrame()**  contains a list of all local variables and stack items and their source i.e. what instruction last manipulated them.
* **basicBlock**: the ```BasicBlock``` context of this instruction
* **methodName**: the method, owner of this instruction
* **className**: the name of the class, owner of this instruction

**```MethodCall```** a special type of instruction that is available only before and after method calls and holds, in addition to the Instruction context, additional fields:
* **methodOwner**: the name of the method owner class
* **methodName**: the name of the method  called
* **currentClassName**: the name of the calling  class
* **methodnode**: the ASM ```MethodInsnNode``` instruction 
* **ins**: references the ```Instruction``` static context object 

**```BasicBlock```** the basic block context provides information about the current basic block, a basic block has a set of instructions it contains the following fields:
* **id**: a unique String id for the block
* **index**: a unique index id for the block in a class
* **blockType**: the block type: Normal, ConditionalJump, Goto, Switch, Return
* **size**: the number of instructions in the block
* **getSuccessorBlocks()**: all connected successor blocks of this block as per the cfg
* **getPredecessorBlocks()**: all connected predecessor blocks of this block as per the cfg
* **getTrueBranch()**: the target block after a conditional jump evaluates to true, is null if the block does not end with  a conditional jump
* **getFalseBranch()**: the target block after a conditional jump evaluates to false, is null if the block does not end with  a conditional jump
* **getFirstInstruction()**: the first ```AbstractInsNode``` in this basic block
* **getLastRealInstruction()**: the last real ```Instruction``` (opcode != -1) in this basic block
* **getFirstRealInstruction()**: the first real ```Instruction``` (opcode != -1) in this basic block
* **method**: the ```Method``` context of this basic block


**```Method```** the method context provides info about the method being instrumented and has the following fields:
* **name**: the name of the method
* **className**: the name of the class owner of the method
* **methodNode**: the ASM org.objectweb.asm.tree.MethodNode
* **getNumberOfBasicBlocks()**: the number of basic blocks in the method
* **getEntryBlock()**: the entry basic block ```BasicBlock```
* **getExitBlocks()**: a list of all exiting basic blocks ```BasicBlock```
* **classContext**: Class context of this method

**```ClassContext```** the class context provides info about the class being instrumented and has the following fields:
* **name**: the name of the method
* **classNode**: the ASM org.objectweb.asm.tree.ClassNode
----

## Dynamic Context

BISM also provides dynamic context objects at selectors to extract joinpoint dynamic information. These objects can access dynamic values from captured joinpoints that are possibly only known during the base program execution. BISM gathers this information from local variables and operand stack, then weaves the necessary code to extract this information.

They are methods that return a special type ```DynamicValue``` which is a reference to a local variable. These local variable are either already created by the compiler or created specially by bism to hold the context requested. Some context is not available at certain join points. For example ```getMethodArgs()``` is only available at ```MethodCall``` joint points. We list the objects that implement these methods, then we show the ```DynamicValue``` type, and then describe these methods:

### Instruction Dynamic Context

```DynamicValue getThreadName(Instruction ins)```
* Returns the name of the thread executing the instruction
* Available in instruction join points.

```DynamicValue getThreadId(Instruction ins)```
* Returns the ID of the thread executing the instruction
* Available in instruction join points.

```LocalArray getStackValues(Instruction ins)```
* Returns an array containing the current values of the stack
* Available in instruction join points.

```DynamicValue getArrayReadValue(Instruction ins)```
* Returns the value of the array element being read by the instruction
* Available in instruction join points.

```DynamicValue getArrayWriteValue(Instruction ins)```
* Returns the value of the array element being written by the instruction
* Available in instruction join points.

```DynamicValue getThis(Instruction ins)```
* Returns a reference to the current object instance
* Available in instruction join points.


### FieldGet Dynamic Context

```DynamicValue getReadValue(FieldAccess fa)```
* Returns the value being read from a field access
* Available in field access join points.

```DynamicValue getFieldOwnerInstance(FieldAccess fa)```
* Returns a reference to the object that owns the field being accessed
* Available in field access join points.


### MethodCallDynamicContext

```LocalArray getAllMethodArgs(MethodCall mc)```
* Returns a reference to a list of all method arguments
* Available in method call join points only.

```DynamicValue getMethodResult(MethodCall mc)```
* Returns a reference to a method result
* Available in method call join points only.

```DynamicValue getMethodReceiver(MethodCall mc)```
* Return a reference to the object whose method is being called. Returns null for static methods.
* Available only in method call join points.

```DynamicValue getThreadName(MethodCall mc)```
* Returns the name of the thread executing the method call
* Available in method call join points.

```DynamicValue getRandomUUID(MethodCall mc)```
* Returns a randomly generated UUID
* Available in method call join points.

```DynamicValue getThreadId(MethodCall mc)```
* Returns the ID of the thread executing the method call
* Available in method call join points.

```DynamicValue getThis(MethodCall mc)```
* Returns a reference to the current object instance
* Available in method call join points.


### Method Dynamic Context

```DynamicValue getThreadName(Method m)```
* Returns the name of the thread executing the method
* Available in method entry and exit join points.

```DynamicValue getRandomUUID(Method m)```
* Returns a randomly generated UUID
* Available in method entry and exit join points.

```DynamicValue getThreadId(Method m)```
* Returns the ID of the thread executing the method
* Available in method entry and exit join points.

```DynamicValue getThis(Method m)```
* Returns a reference to the current object instance
* Available in method entry and exit join points.


### FieldSet Dynamic Context

```DynamicValue getFieldOwnerInstance(FieldAccess fa)```
* Returns a reference to the object that owns the field being written
* Available in field access join points.

```DynamicValue getWriteValue(FieldAccess fa)```
* Returns the value being written to a field access
* Available in field access join points.


### FieldAccess Common Dynamic Context

```DynamicValue getThreadName(FieldAccess fa)```
* Returns the name of the thread accessing the field
* Available in field access join points.

```DynamicValue getThreadId(FieldAccess fa)```
* Returns the ID of the thread accessing the field
* Available in field access join points.

```DynamicValue getThis(FieldAccess fa)```
* Returns a reference to the current object instance

### Basic Block Dynamic Context

```DynamicValue getThreadName(BasicBlock bb)```
* Returns the name of the thread executing the basic block
* Available in basic block join points.

```DynamicValue getThreadId(BasicBlock bb)```
* Returns the ID of the thread executing the basic block
* Available in basic block join points.

```DynamicValue getArrayReadValue(BasicBlock bb)```
* Returns the value of the array element being read by the basic block
* Available in basic block join points.

```DynamicValue getArrayWriteValue(BasicBlock bb)```
* Returns the value of the array element being written by the basic block
* Available in basic block join points.

```DynamicValue getThis(BasicBlock bb)```
* Returns a reference to the current object instance
* Available in basic block join points.

 