 #  Tasks

To complete the exercises, you first need to clone this repository onto your local machine. Additionally, make sure that you have `Java (17 or lower)` installed on your system.

 

## Samples




### DSL Example  

Instrumenting a program generally involves specifying three key requirements: (1) program points to capture, (2) the information needed from these events, and (3) the consumption of these events. The DSL addresses these requirements by providing these three main constructs: (1) `pointcut` to capture points of interest in the program, (2)  `event` to specify the required information from the captured events, and (3)  `monitor` to consume the captured events.

 
```java
pointcut pc1 after MethodCall(* *.*List.iterator(..))

event e1("create", [getMethodReceiver, getMethodResult]) on pc1 

monitor m1{
    class: Monitor,
    events: [e1 to receiveEvent(String, Object, java.util.Iterator)]
}    
```

This code will weave into the base program the code needed to invoke the monitor after the method call.

The transformer uses the selector `after MethodCall` to capture the return of an  `Iterator` created from a  `List.iterator()`  method call.

Note that in `* *.*List.iterator(..)`, the first `*` is the return type, the second `*` is the package name, and the third `*` is the class name. The `..` means that the method can have any number of arguments.

The event defines a constant string literal `create` to denote the name of the event and extracts the dynamic context objects: `getMethodReceiver` returns the object on which the method was called, and `getMethodResult` returns the result of the method call.

Then, invokes a class called `Monitor` passing the extracted information as parameters to `receiveEvent(..)` method.




### API Example  
```java
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.DynamicValue;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;

public class MyTransformer extends Transformer {
 @Override
 public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc){
    // Filter to only method of interest
    if (mc.methodName.equals("iterator") && mc.methodOwner.contains("List")) {
       
        // Access to dynamic data, mc is needed so BISM can extract dynamic context objects
        DynamicValue list = dc.getMethodReceiver(mc);
        DynamicValue iterator = dc.getMethodResult(mc);   
       
      
        // Invoke the monitor after passing arguments
        StaticInvocation sti = 
            new StaticInvocation("Monitor", "receiveEvent");
        sti.addParameter("create");          
        sti.addParameter(list);
        sti.addParameter(iterator);
        invoke(sti);
    }
 } 
}
```

You can run the samples by following the instructions below.
 
## Organization of Exercises  

This `exercises` folder contains various subdirectories and files as described below:

- Each property has its own folder which is named after its number and mode (API or DSL).
   - Inside each folder:
      - There is config file that is needed for BISM, it specifies the instrumentation sources and the output directory which dumps the instrumented files.
      - A Makefile which is needed to run the below commands.
- `sample-dsl` and `sample-api` folders have samples for both modes.
- `fits`: contains the source code for the FiTS application.
- `bism`: contains `bism.jar`.
- `Monitor.java`: contains the monitor class.


## How to Run 

The repo already contains the needed code to compile the monitor and execute BISM.



Navigate to the respective directory.

```
cd ./exercises/property1-api

```
**After implementing the transformer, you can run the following commands to compile and execute the FiTS application with the specified monitor and transformer.**
 

```
make compile
```

This will compile the transformer you wrote, the FiTS sources, and the monitor class. `Transformers written in DSL will not be compiled as they are interpreted at runtime.`

To execute the FiTS application with the specified monitor and transformer, run the command:

```
make run
```

This attaches BISM to the application and performs instrumentation at load-time.

Finally, if you want to clean up the project, you can run the following command:

```
make clean
```

This will remove all the compiled files and restore the project to its initial state.
