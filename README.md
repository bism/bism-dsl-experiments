# BISM DSL Experiments

In this repo we provide the code for the experiments of the BISM DSL paper. The experiments are divided into two parts: (1) performance evaluation and (2) user experience evaluation.  
 
 Navigate to the respective folders for more information on how to run the experiments.