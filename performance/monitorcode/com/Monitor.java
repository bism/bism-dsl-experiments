package com;

import java.util.List;

public class Monitor {

    private static int eventCount = 0;

    public static void receiveEvents(String eventName, List<Object> eventData) {
        //  System.out.println("Received event " + eventName + " with data: " + eventData);
        eventCount++;
    }

    public static int getEventCount() {
        return eventCount;
    }

   
    public static void stats() {
        System.out.println("Received events " + eventCount );
    }

    
    public static void statsB(int g) {
        System.out.println("Received events " + eventCount );
    }


}
