import inria.bism.transformers.*;
import java.util.*;
import inria.bism.transformers.dynamiccontext.*;
import inria.bism.transformers.staticcontext.*;

public class BismTransformer  extends Transformer  {

public void afterMethodCall(MethodCall sc, MethodCallDynamicContext dc) {
    if (sc.methodName.contains("next") && sc.methodOwner.contains("Iterator")) {
        
        StaticInvocation sti1 = new StaticInvocation("com/Monitor", "receiveEvents");
        
        List<Object> objectList1 = new ArrayList<>();
        objectList1.add(sc.fullName);
        LocalArray la1 = dc.createLocalArray(sc.ins.basicBlock.method, Object.class);
        dc.addToLocalArray(la1,dc.getAllMethodArgs(sc));
        dc.addToLocalArray(la1,dc.getMethodReceiver(sc));
        dc.addToLocalArray(la1,dc.getThis(sc));
        objectList1.add(la1);
        sti1.addParameter(objectList1.get(0));
        sti1.addParameter((DynamicValue) objectList1.get(1));
        invokeWithSpec(sti1, "String,java.util.List");
    }
    if (sc.methodName.contains("get")  ) {
        
        StaticInvocation sti2 = new StaticInvocation("com/Monitor", "receiveEvents");
        
        List<Object> objectList2 = new ArrayList<>();
        objectList2.add(sc.fullName);
        LocalArray la2 = dc.createLocalArray(sc.ins.basicBlock.method, Object.class);
        dc.addToLocalArray(la2,dc.getAllMethodArgs(sc));
        dc.addToLocalArray(la2,dc.getMethodReceiver(sc));
        dc.addToLocalArray(la2,dc.getThis(sc));
        objectList2.add(la2);
        sti2.addParameter(objectList2.get(0));
        sti2.addParameter((DynamicValue) objectList2.get(1));
        invokeWithSpec(sti2, "String,java.util.List");
    }
    
        
        StaticInvocation sti3 = new StaticInvocation("com/Monitor", "receiveEvents");
        
        List<Object> objectList3 = new ArrayList<>();
        objectList3.add(sc.fullName);
        LocalArray la3 = dc.createLocalArray(sc.ins.basicBlock.method, Object.class);
        dc.addToLocalArray(la3,dc.getAllMethodArgs(sc));
        dc.addToLocalArray(la3,dc.getMethodReceiver(sc));
        dc.addToLocalArray(la3,dc.getThis(sc));
        objectList3.add(la3);
        sti3.addParameter(objectList3.get(0));
        sti3.addParameter((DynamicValue) objectList3.get(1));
        invokeWithSpec(sti3, "String,java.util.List");
     
        
}

public void onMethodExit(Method sc, MethodDynamicContext dc) {
    if (sc.name.contains("main") && sc.className.contains("Main")) {
        
        StaticInvocation sti4 = new StaticInvocation("com/Monitor", "statsB");
        
        List<Object> objectList4 = new ArrayList<>();
        objectList4.add(Integer.parseInt("5"));
        sti4.addParameter(objectList4.get(0));
        invokeWithSpec(sti4, "int");
    }
        
}

}