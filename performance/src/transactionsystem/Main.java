package transactionsystem;

public class Main {

    public static void main(String[] args) {

        int x = 3;
        int y = 5;

        if (x < y) {
            y = 0;
            x = x + 1;
        } else {
            x = y;
        }

//
        System.out.println("Running all scenarios");
        Scenarios.runAllScenarios();
        System.out.println("Completed");

        //		A();
//
		A();

    }

    private static void A() {
        C(false);
        B(true);
        C(true);
        D();

    }
    private static void B(boolean e) {
        if(e){
            E();
        }
    }
    private static void C(boolean e) {

        try {
            Thread.sleep(20*10);
        } catch(InterruptedException g) { }

        if(e){
            E();
        }
    }
    private static void D() {
        F();
    }
    private static void E() {
//		A();
    }

    private static void F() {
//		A();
    }

}
