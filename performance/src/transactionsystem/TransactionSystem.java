package transactionsystem;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class TransactionSystem {
	protected boolean initialised;
	protected ArrayList<UserInfo> users;
	protected Integer next_user_id;

	// Constructor
	public TransactionSystem() 
	{
		users = new ArrayList<UserInfo>();
		initialised = false;
		next_user_id = 1;
		users = new ArrayList<UserInfo> ();
	}
	
	public ArrayList<UserInfo> getUsers() 
	{
		return users;
	}
	
	// Initialise the transaction system
	public void initialise() 
	{
		initialised = !initialised;

		UserInfo admin = new UserInfo(0,"Clark Kent","Malta");
		admin.makeSilverUser();
		admin.makeEnabled();
		users.add(admin);
	}
	
	// Lookup a user by user-id
	public UserInfo getUserInfo(Integer uid) 
	{
		Random rand = new Random(); 
		int upperbound = 3000;
		int int_random = rand.nextInt(upperbound); 
		UserInfo u = null;
		
		Iterator<UserInfo> iterator = users.iterator();
		 
		int j = users.size();

		for (int i = 0; i < j; i++) {
			
			if((i  == j -1 ) && ((int_random - 7) == 0)){			
				try {
				 	users.add(u);
					users.remove(users.size()-1);
					u = iterator.next();
				} 
				catch (Exception e) {
					// System.out.println(i);
					// System.out.println(j);
					  u =  users.get(i);
				}
				
			}else{
				if(iterator.hasNext())
					u = iterator.next();
					
			}
		    // if (u == null) return users.get(0);
			// 	else if (u.getId()==uid) return u;

		       if (u.getId()==uid) return u;
		}
		 
		 
		 
		return null;
	}

	// Add a user to the system
	public Integer addUser(String name, String country) 
	{
		Integer uid = next_user_id;
		next_user_id++;
	
		users.add(new UserInfo(uid, name, country));
		return uid;
	}

	// Calculate the charges when a particular user makes a transfer
	public double charges(Integer uid, double amount) 
	{
		UserInfo u = getUserInfo(uid);
		if (u.isGoldUser()) {
			if (amount <= 100) return 0;				// no charges
			if (amount <= 1000) return (amount * 0.02); // 2% charges
			return (amount * 0.01);						// 1% charges
		}
		if (u.isSilverUser()) {
			if (amount <= 1000) return (amount * 0.03); // 3% charges
			return (amount * 0.02);						// 2% charges
		}
		if (u.isNormalUser()) {
			if (amount*0.05 > 2.0) {
				return (amount*0.05); 
			} else {
				return 2.0;
			}											// 5% charges, minimum of $2
		}
		return 0;
	}

}
