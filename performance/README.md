# Performance Assessment for BISM DSL

This experiment aims to evaluate the performance of BISM DSLaj, compared to traditional BISM and AspectJ approaches. We use the financial transaction system to extract events from method calls, field operations, and method executions.

We assess the performance of four different approaches: (1) **AspectJ DSL**, which is implemented by writing an aspect (.aj) with the DSL; (2) **AspectJ Java**, which is implemented by writing an aspect in Java syntax using annotations; (3) **BISM DSL**, which is our proposed DSL developed for BISM; and (4) **BISM API**, which is implemented by writing classical BISM transformers in Java.

## Prerequisites

Before running the benchmarks, you need to install the following dependencies:


### Java

You will also need Java installed on your system. You can download and install Java from the [official Oracle website](https://www.oracle.com/java/technologies/javase-downloads.html) or use OpenJDK (tested for JDK17 and below).

### Python

You will need Python installed on your system to generate a plot from the benchmark results. You can download and install Python from the [official Python website](https://www.python.org/downloads/) or use a package manager like Anaconda.

### Hyperfine

If you want to run the **full** benchmark you need to install Hyperfine.
Hyperfine is a command-line benchmarking tool. To install it, follow the instructions in the [official Hyperfine repository](https://github.com/sharkdp/hyperfine).

If you want to run each benchmark individually, you do not need it.



## Running the Benchmarks
 

First you need to prepare the benchmarks by executing:
```bash
make prepare
```
To run the benchmarks individually, you can use the following commands in the project directory:

- **AspectJ DSL:** 
```bash
make ajc_run
```
This command runs the benchmark for AspectJ DSL. It involves writing an aspect file (`.aj`) using the AspectJ DSL and executing it.

- **AspectJ API:** 
```bash
make aj_run
```
This command runs the benchmark for AspectJ API. It involves writing an aspect file in Java syntax using annotations and executing it.

- **BISM DSL:** 
```bash
make bism_dsl
```
This command runs the benchmark for BISM DSL. It involves using our proposed DSL (Domain-Specific Language) developed for BISM (Behavior-Integrated State Machines).

- **BISM API:** 
```bash
make bism_api
```
This command runs the benchmark for BISM API. It involves writing classical BISM transformers and executing them.
 

- **Full Benchmark** 

To run the benchmarks, run the following command in the project directory:

```bash
make benchmark
```

This command executes the Hyperfine command specified in the Makefile, and the results are saved in `results.json` and `bench.md`.

### Generating a Plot

After running the benchmarks, you can generate a plot to visualize the results by executing the `plot.py` Python script. The plot shows the average time taken by each approach to the instrument and executes the program. 

To generate the plot, ensure that you have Python installed on your system, and run the following command in the project directory:

```bash
python plot.py
```

The plot will be saved in a file named `execution.pdf` in the project directory.  

### Cleaning up Generated Code

To remove the generated code from the project directory, run the following command:

```bash
make clean
```

This command removes any files generated during the benchmarking process.  