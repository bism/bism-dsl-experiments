import json
import matplotlib.pyplot as plt

# Load data from JSON file
with open('results.json') as f:
    data = json.load(f)

# Extract the relevant data for plotting
commands = [result["command"] for result in data["results"]]
means = [result["mean"] for result in data["results"]]
stddevs = [result["stddev"] for result in data["results"]]

# Change the values inside the commands list
commands[0] = "AspectJ DSL"
commands[1] = "AspectJ API"
commands[2] = "BISM DSL"
commands[3] = "BISM API"

# Get the color cycle
color_cycle = plt.rcParams["axes.prop_cycle"].by_key()["color"]

# Set up the bar chart
fig, ax = plt.subplots(figsize=(6, 6)) 
bar_positions = range(len(commands))
ax.bar(bar_positions, means, yerr=stddevs, align='center', alpha=0.5, ecolor='black', capsize=10, width=0.5, color=[color_cycle[3], color_cycle[3], color_cycle[0], color_cycle[0]])

# Customize the chart
ax.set_xticks(bar_positions)
ax.set_xticklabels(commands, fontsize=16)
ax.yaxis.grid(True)

ax.set_aspect(1)

# Show the chart
plt.tight_layout()
plt.savefig('execution.pdf', dpi=300) 
