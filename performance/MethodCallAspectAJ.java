import java.util.*;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class MethodCallAspectAJ {
    @Pointcut("execution(* *.main(..))")
    public void lastMethod() {}

    @After("lastMethod()")
    public void afterLastMethod() {
        com.Monitor.stats();
    }

    @Pointcut("call(* java.util.Iterator.next()) && within(transactionsystem..*)")
    public void mapCollectionIteratorMethods() {}

    @After("mapCollectionIteratorMethods()")
    public void afterMapCollectionIteratorMethods(JoinPoint thisJoinPoint) {
     
            // System.out.println(thisJoinPoint.toString()+"");
            List<Object> li = new ArrayList<>();
            li.add(thisJoinPoint.getArgs());
            li.add(thisJoinPoint.getTarget());
            li.add(thisJoinPoint.getThis());
            com.Monitor.receiveEvents(thisJoinPoint.toString() + "", li);
       
    }

    @Pointcut("call(* *.get*(..)) && within(transactionsystem..*)")
    public void getMethods() {}

    @After("getMethods()")
    public void afterGetMethods(JoinPoint thisJoinPoint) {

            // System.out.println(thisJoinPoint.toString()+"");
            List<Object> li = new ArrayList<>();
            li.add(thisJoinPoint.getArgs());
            li.add(thisJoinPoint.getTarget());
            li.add(thisJoinPoint.getThis());
            com.Monitor.receiveEvents(thisJoinPoint.toString() + "", li);

    }

    @Pointcut("call(* *.*(..)) && within(transactionsystem..*)")
    public void allMethods() {}

    @After("allMethods()")
    public void afterAllMethods(JoinPoint thisJoinPoint) {
    
            // System.out.println(thisJoinPoint.toString()+"");
            List<Object> li = new ArrayList<>();
            li.add(thisJoinPoint.getArgs());
            li.add(thisJoinPoint.getTarget());
            li.add(thisJoinPoint.getThis());
            com.Monitor.receiveEvents(thisJoinPoint.toString() + "", li);
     
    }

}

// import org.aspectj.lang.JoinPoint;
// import org.aspectj.lang.annotation.Aspect;
// import org.aspectj.lang.annotation.*;
// import org.aspectj.lang.annotation.Pointcut;

// @Aspect
// public class MethodCallAspect {

//     @Pointcut("execution(* *(..)) && !within(MethodCallAspect)")
//     public void methodCall() {}

//     @Before("methodCall()")
//     public void logMethodCall(JoinPoint joinPoint) {
//         System.out.println("Before Method called: " + joinPoint.getSignature());
//     }

//     @After("methodCall()")
//     public void logMethodCallA(JoinPoint joinPoint) {
//         System.out.println("After Method called: " + joinPoint.getSignature());
//     }

// }
