pointcut pc1 after MethodCall(* *.Iterator.next())  

pointcut pc2 after MethodCall(* *.get*(..))  

pointcut pc3 after MethodCall(* *.*(..))  

pointcut pc4 on MethodExit(* *.Main.main(..))      

event e1(name, [getAllMethodArgs,getMethodReceiver,getThis])
            on pc1 to com.Monitor.receiveEvents(String,List)

event e2(name, [getAllMethodArgs,getMethodReceiver,getThis])
            on pc2 to com.Monitor.receiveEvents(String,List)

event e3(name, [getAllMethodArgs,getMethodReceiver,getThis])
            on pc3 to com.Monitor.receiveEvents(String,List)

event e4(5) on pc4 to com.Monitor.statsB(int)


 