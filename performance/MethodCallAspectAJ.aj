import java.util.*;
import org.aspectj.lang.*; 


public aspect MethodCallAspectAJ {
   
    pointcut lastMethod() : execution(* *.main(..));

    after() : lastMethod() {
        com.Monitor.stats();   
    } 

    after() :  (call(* Iterator.next()) ) && within(transactionsystem..*)  
    {
        List li = new ArrayList<Object>();
        li.add(thisJoinPoint.getArgs());
        li.add(thisJoinPoint.getTarget());
        li.add(thisJoinPoint.getThis());
        com.Monitor.receiveEvents(thisJoinPoint.toString(), li);
    }

     after() :  call(* *.get*(..))  && within(transactionsystem..*)  
    {
        List li = new ArrayList<Object>();
        li.add(thisJoinPoint.getArgs());
        li.add(thisJoinPoint.getTarget());
        li.add(thisJoinPoint.getThis());
        com.Monitor.receiveEvents(thisJoinPoint.toString(), li);
    }

     after() :  call(* *.*(..))  && within(transactionsystem..*)  
    {
        List li = new ArrayList<Object>();
        li.add(thisJoinPoint.getArgs());
        li.add(thisJoinPoint.getTarget());
        li.add(thisJoinPoint.getThis());
        com.Monitor.receiveEvents(thisJoinPoint.toString(), li);
    }

}
